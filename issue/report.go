package issue

import "sort"

// Report is the output of an analyzer.
type Report struct {
	Version         Version `json:"version"`
	Vulnerabilities []Issue `json:"vulnerabilities"`
}

// Sort sorts vulnerabilites by decreasing severity.
func (r *Report) Sort() {
	sort.Slice(r.Vulnerabilities, func(i, j int) bool {
		return r.Vulnerabilities[i].Severity > r.Vulnerabilities[j].Severity
	})
}

// Dedupe removes duplicates from vulnerabilities
func (r *Report) Dedupe() {
	r.Vulnerabilities = Dedupe(r.Vulnerabilities...)
}

// NewReport creates a new report in current version.
func NewReport() Report {
	return Report{
		Version:         CurrentVersion(),
		Vulnerabilities: []Issue{},
	}
}

// MergeReports merges the given reports and bring them to the current syntax version.
func MergeReports(reports ...Report) Report {
	report := NewReport()
	for _, r := range reports {
		report.Vulnerabilities = append(report.Vulnerabilities, r.Vulnerabilities...)
	}
	report.Dedupe()
	report.Sort()
	return report
}
