package issue

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"
)

var testSASTVulnerability = Issue{
	Category:    CategorySast,
	Name:        "Possible command injection",
	Message:     "Possible command injection in application_controller",
	Description: "The ciphertext produced is susceptible to alteration by an adversary. This mean that the cipher provides no wayto detect that the data has been tampered with. If the ciphertext can be controlled by an attacker, it couldbe altered without detection.\n\nThe solution is to used a cipher that includes a Hash basedMessage Authentication Code (HMAC) to sign the data. Combining a HMAC function to the existing cipher is proneto error [1] ( http://www.thoughtcrime.org/blog/the-cryptographic-doom-principle/ ).Specifically, it is always recommended that you be able to verify the HMAC first, and only if the data is unmodified,do you then perform any cryptographic functions on the data.\n\nThe following modes are vulnerablebecause they don't provide a HMAC:",
	CompareKey:  "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
	Location: Location{
		File:      "app/controllers/application_controller.rb",
		LineStart: 831,
		LineEnd:   832,
	},
	Scanner: Scanner{
		ID:   "brakeman",
		Name: "Brakeman",
	},
	Severity:   LevelMedium,
	Confidence: LevelHigh,
	Solution:   "Use the system(command, parameters) method which passes command line parameters safely.",
	Identifiers: []Identifier{
		CVEIdentifier("CVE-2018-1234"),
	},
	Links: []Link{
		{
			Name: "Awesome-security blog post",
			URL:  "https://example.com/blog-post",
		},
		{
			URL: "https://example.com/another-blog-post",
		},
	},
}

var testSASTVulnerabilityJSON = `{
  "category": "sast",
  "name": "Possible command injection",
  "message": "Possible command injection in application_controller",
  "description": "The ciphertext produced is susceptible to alteration by an adversary. This mean that the cipher provides no wayto detect that the data has been tampered with. If the ciphertext can be controlled by an attacker, it couldbe altered without detection.\n\nThe solution is to used a cipher that includes a Hash basedMessage Authentication Code (HMAC) to sign the data. Combining a HMAC function to the existing cipher is proneto error [1] ( http://www.thoughtcrime.org/blog/the-cryptographic-doom-principle/ ).Specifically, it is always recommended that you be able to verify the HMAC first, and only if the data is unmodified,do you then perform any cryptographic functions on the data.\n\nThe following modes are vulnerablebecause they don't provide a HMAC:",
  "cve": "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
  "severity": "Medium",
  "confidence": "High",
  "solution": "Use the system(command, parameters) method which passes command line parameters safely.",
  "scanner": {
    "id": "brakeman",
    "name": "Brakeman"
  },
  "location": {
    "file": "app/controllers/application_controller.rb",
    "start_line": 831,
    "end_line": 832,
    "dependency": {
      "package": {}
    }
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ],
  "links": [
    {
      "name": "Awesome-security blog post",
      "url": "https://example.com/blog-post"
    },
    {
      "url": "https://example.com/another-blog-post"
    }
  ]
}`

var testDependencyScanningVulnerability = DependencyScanningVulnerability{Issue{
	Category: CategoryDependencyScanning,
	Scanner: Scanner{
		ID:   "gemnasium",
		Name: "Gemnasium",
	},
	Location: Location{
		File: "app/pom.xml",
		Dependency: Dependency{
			Package: Package{
				Name: "io.netty/netty",
			},
			Version: "3.9.1.Final",
		},
	},
	Identifiers: []Identifier{
		CVEIdentifier("CVE-2018-1234"),
	},
}}

var testDependencyScanningVulnerabilityJSON = `{
  "category": "dependency_scanning",
  "message": "Vulnerability in io.netty/netty",
  "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
  "scanner": {
    "id": "gemnasium",
    "name": "Gemnasium"
  },
  "location": {
    "file": "app/pom.xml",
    "dependency": {
      "package": {
        "name": "io.netty/netty"
      },
      "version": "3.9.1.Final"
    }
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ]
}`

var testDependencyScanningIssueJSON = `{
  "category": "dependency_scanning",
  "scanner": {
    "id": "gemnasium",
    "name": "Gemnasium"
  },
  "location": {
    "file": "app/pom.xml",
    "dependency": {
      "package": {
        "name": "io.netty/netty"
      },
      "version": "3.9.1.Final"
    }
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ]
}`

const testEmptyIssueJSON = `{
  "category": "",
  "cve": "",
  "scanner": {
    "id": "",
    "name": ""
  },
  "location": {
    "dependency": {
      "package": {}
    }
  },
  "identifiers": null
}`

func TestIssue(t *testing.T) {

	t.Run("MarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name          string
			Vulnerability interface{}
			JSON          string
		}{
			{
				Name:          "SAST",
				Vulnerability: testSASTVulnerability,
				JSON:          testSASTVulnerabilityJSON,
			},
			{
				Name:          "DS",
				Vulnerability: testDependencyScanningVulnerability.ToIssue(),
				JSON:          testDependencyScanningVulnerabilityJSON,
			},
			{
				Name:          "Empty",
				Vulnerability: Issue{},
				JSON:          testEmptyIssueJSON,
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				b, err := json.Marshal(tc.Vulnerability)
				if err != nil {
					t.Fatal(err)
				}

				var buf bytes.Buffer
				json.Indent(&buf, b, "", "  ")
				got := buf.String()

				if got != tc.JSON {
					t.Errorf("Wrong JSON output. Expected:\n%s\nBut got:\n%s", tc.JSON, got)
				}
			})
		}
	})

	t.Run("UnmarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name  string
			JSON  string // JSON encoded vulnerability
			Issue Issue  // Issue to be JSON decoded
		}{
			{
				Name:  "SAST",
				JSON:  testSASTVulnerabilityJSON,
				Issue: testSASTVulnerability,
			},
			{
				Name:  "DS",
				JSON:  testDependencyScanningIssueJSON,
				Issue: testDependencyScanningVulnerability.Issue,
			},
			{
				Name:  "Empty",
				JSON:  testEmptyIssueJSON,
				Issue: Issue{},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				var got Issue
				if err := json.Unmarshal([]byte(tc.JSON), &got); err != nil {
					t.Fatal(err)
				}

				if !reflect.DeepEqual(got, tc.Issue) {
					t.Errorf("Wrong unmarshalled Issue. Expected:\n%#v\nBut got:\n%#v", tc.Issue, got)
				}
			})
		}
	})
}
