package issue

import (
	"reflect"
	"testing"
)

func TestNewReport(t *testing.T) {
	got := NewReport().Version
	want := CurrentVersion()
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong version. Expecting %v but got %v", want, got)
	}
}

// NOTE: Sort is tested when testing MergeReports

func TestMergeReports(t *testing.T) {
	location := Location{File: "app/Gemfile.lock"}
	ids := []Identifier{CVEIdentifier("CVE-2018-14404")}

	reports := []Report{
		{
			Version: Version{Major: 2, Minor: 3},
			Vulnerabilities: []Issue{
				{Name: "R1/low", Severity: LevelLow},
				{
					Name:        "R1/critical",
					Severity:    LevelCritical,
					Location:    location,
					Identifiers: ids,
				},
			},
		},
		{
			Version: Version{Major: 2, Minor: 4},
			Vulnerabilities: []Issue{
				{Name: "R2/low", Severity: LevelLow},
				{Name: "R2/critical", Severity: LevelCritical},
			},
		},
		{
			Version: Version{Major: 2, Minor: 5},
			Vulnerabilities: []Issue{
				{Name: "R3/low", Severity: LevelLow},
				{Name: "R3/critical", Severity: LevelCritical},
				{
					Name:        "R3/critical/dup",
					Severity:    LevelCritical,
					Location:    location,
					Identifiers: ids,
				},
			},
		},
	}
	want := Report{
		Version: CurrentVersion(),
		Vulnerabilities: []Issue{
			{
				Name:        "R1/critical",
				Severity:    LevelCritical,
				Location:    location,
				Identifiers: ids,
			},
			{Name: "R2/critical", Severity: LevelCritical},
			{Name: "R3/critical", Severity: LevelCritical},
			{Name: "R1/low", Severity: LevelLow},
			{Name: "R2/low", Severity: LevelLow},
			{Name: "R3/low", Severity: LevelLow},
		},
	}

	got := MergeReports(reports...)
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nGot:%#v", want, got)
	}
}
