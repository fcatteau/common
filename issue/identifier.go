package issue

import (
	"fmt"
	"strconv"
	"strings"
)

type IdentifierType string

const (
	IdentifierTypeCVE   IdentifierType = "cve"
	IdentifierTypeCWE   IdentifierType = "cwe"
	IdentifierTypeOSVDB IdentifierType = "osvdb"
	IdentifierTypeUSN   IdentifierType = "usn"
)

type Identifier struct {
	Type  IdentifierType `json:"type"`          // Type of the identifier (CVE, CWE, VENDOR_X, etc.)
	Name  string         `json:"name"`          // Name of the identifier for display purpose
	Value string         `json:"value"`         // Value of the identifier for matching purpose
	URL   string         `json:"url,omitempty"` // URL to identifier's documentation
}

func ParseIdentifierID(idStr string) (Identifier, bool) {
	parts := strings.SplitN(idStr, "-", 2)
	switch strings.ToUpper(parts[0]) {
	case "CVE":
		return CVEIdentifier(idStr), true
	case "CWE":
		if idInt, err := strconv.Atoi(parts[1]); err == nil {
			return CWEIdentifier(idInt), true
		}
	case "OSVDB":
		return OSVDBIdentifier(idStr), true
	case "USN":
		return USNIdentifier(idStr), true
	}
	return Identifier{}, false
}

// CVEIdentifier returns a structured Identifier for a given CVE-ID
// Given ID must follow this format: `CVE-YYYY-NNNNN`
func CVEIdentifier(ID string) Identifier {
	return Identifier{
		Type:  IdentifierTypeCVE,
		Name:  ID,
		Value: ID,
		URL:   fmt.Sprintf("https://cve.mitre.org/cgi-bin/cvename.cgi?name=%s", ID),
	}
}

// CWEIdentifier returns a structured Identifier for a given CWE ID
// Given ID must follow this format: `NNN` (just the number, no prefix)
func CWEIdentifier(ID int) Identifier {
	return Identifier{
		Type:  IdentifierTypeCWE,
		Name:  fmt.Sprintf("CWE-%d", ID),
		Value: strconv.Itoa(ID),
		URL:   fmt.Sprintf("https://cwe.mitre.org/data/definitions/%d.html", ID),
	}
}

// OSVDBIdentifier returns a structured Identifier for a given OSVDB-ID
// Given ID must follow this format: `OSVDB-XXXXXX`
func OSVDBIdentifier(ID string) Identifier {
	return Identifier{
		Type:  IdentifierTypeOSVDB,
		Name:  ID,
		Value: ID,
		URL:   "https://cve.mitre.org/data/refs/refmap/source-OSVDB.html",
	}
}

// USNIdentifier returns a structured Identifier for a Ubuntu Security Notice.
// Given ID must follow this format: `USN-XXXXXX`.
func USNIdentifier(ID string) Identifier {
	parts := strings.SplitN(ID, "-", 2)
	return Identifier{
		Type:  IdentifierTypeUSN,
		Name:  ID,
		Value: ID,
		URL:   fmt.Sprintf("https://usn.ubuntu.com/%s/", parts[1]),
	}
}
